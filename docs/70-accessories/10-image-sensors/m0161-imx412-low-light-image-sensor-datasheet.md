---
layout: default
title: M0161 IMX412 Low Light Image Sensor Datasheet
parent: Image Sensors
nav_order: 161
has_children: false
permalink: /M0161/
---

# VOXL Hires Sensor Datasheet

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

### Hi-resolution, 4k M0107 m12 IMX412 120° FOV ([Buy Here](https://www.modalai.com/products/msu-m0107))

| Specification | Value                                                                                          |
|----------------|------------------------------------------------------------------------------------------------|
| Sensor         | IMX412 [Datasheet](https://www.sony-semicon.co.jp/products/common/pdf/IMX412-AACK_Flyer03.pdf) |
| Shutter        | Rolling                                                                                        |
| Max Resolution | 7.857 mm (Type 1/2.3) 12.3 Mega-pixel                                                          |
| Framerate      | TBD                                                                                            |
| Lens Mount     | m12                                                                                            |
| Lens Part No.  | 27629F-16MAS-CM                                                                                |
| Focusing Range | TBD                                                                                            |
| Focal Length   | 2.7mm                                                                                          |
| F Number       | TBD                                                                                            |
| Fov(DxHxV)     | 120.4° x 93.5° x 146°                                                                          |
| TV Distortion  | TBD                                                                                            |
| Weight         | 7.5g                                                                                           |
| IR Filter      | Optional                                                                                       |


## Mechanical Drawings (3D Step)

## Module Connector Schematic for J2

## Image Samples for Sensor

### Indoor

### Outdoor

## VOXL 2 Integration

SDK 1.3.0 and greater

More information [here](/voxl2-camera-configs/)
