---
layout: default
title: M0015 Stereo Module Datasheet
parent: Image Sensors
nav_order: 15
has_children: false
permalink: /M0015/
---

# VOXL Stereo Sensor Datasheet
{: .no_toc }

## Specification

### MSU-M0015-1-01

| Specicifcation | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV7251 [Datasheet](https://www.ovt.com/download/sensorpdf/146/OmniVision_OV7251.pdf) |
| Shutter        | Global, Hardware-Synchronized                                                        |
| Resolution     | 1280x480 (640x480 * 2)                                                               |
| Framerate      | up to 60Hz                                                                           |
| Data formats   | YUV only                                                                             |
| Lens Size      | 1/7.5"                                                                               |
| Focusing Range | 5cm~infinity                                                                         |
| Focal Length   | 1.77mm                                                                               |
| F Number       | 2.5                                                                                  |
| Fov(DxHxV)     | 85° x 68° x 56°                                                                      |
| TV Distortion  | < -3.5%                                                                              |
| Weight         | 4g                                                                                   |


#### M0015-1 2D / 3D Drawings

3D model can be found as a component in the VOXL Flight Deck STEP file [found here](https://developer.modalai.com/asset/eula-download/55)

[2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0015_2D_11-02-21.pdf)

### MSU-M0113-1 

| Specicifcation | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV9782                                                                               |

## Pinouts

The following diagram shows the connectors on the [MSU-M0010-1-01](https://www.modalai.com/products/voxl-replacement-stereo-flex-cable) stereo flex cable. The OEM-M0015-1-00 image sensor module connects to these for example.

![MSU-M0010-1-01-pinout.png](/images/other-products/image-sensors/MFPC-M0010A-pinout.png)

![Stereo_TFlex_Pin_Location.JPG](/images/other-products/image-sensors/Stereo_TFlex_Pin_Location.JPG)

![Stereo_TFlex_Complete.JPG](/images/other-products/image-sensors/Stereo_TFlex_Complete.JPG)

![Stereo_TFlex_Location.JPG](/images/other-products/image-sensors/Stereo_TFlex_Location.JPG)

## Samples of Stereo Front and Rear sensors on Sentinel.
### Indoor
Stereo-front left
{:target="_blank"}
![stereo_front_l_in.png](/images/other-products/image-sensors/Samples/stereo_front_l_in.png)
{:target="_blank"}
Stereo-front right
{:target="_blank"}
![stereo_front_r_in.png](/images/other-products/image-sensors/Samples/stereo_front_r_in.png)

### Outdoor
Stereo-front left
{:target="_blank"}
![stereo_front_l_out.png](/images/other-products/image-sensors/Samples/stereo_front_l_out.png)
{:target="_blank"}
Stereo-front right 
{:target="_blank"}
![stereo_front_r_out.png](/images/other-products/image-sensors/Samples/stereo_front_r_out.png)
