---
layout: default
title: M0171 VOXL 2 Time of Flight Module Datasheet
parent: Image Sensors
nav_order: 171
has_children: false
permalink: /M0171/
---

# VOXL 2 Time of Flight (ToF) Sensor Datasheet (IRS2975C)
{: .no_toc }

## Specification

The PMD Time of Flight sensor produces high-fidelity depth mapping indoors up to 6m. 

The [M0171-1](/M0171/) ToF adapter has a new ModalAI 40-pin format Molex connector and includes a high-power 5V feed as part of the single connector. This is a new connector not used elsewhere in ModalAI, so some integration guidelines are required as explained below.

The M0171-1 adapter can be used with the M0170-1 40-pin extension flex and requires a PCB adapter at VOXL 2 (such as M0172 or M0173) that exposes this new 40-pin Molex from a Camera Group connector. 



### Requirements

* VOXL (APQ8096) not supported.
* VOXL 2 (QRB5165) Requires SDK 1.2 or greater
* * New 40-pin Molex converter board and flex cable are needed

### Details

| Specification    | Value                                                                                                                                                                    |
|-------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Part Number       | MSU-M0171-1-01                                                                                                                                                           |
| Technology        | [PMD](https://www.pmdtec.com/)                                                                                                                                           |
| Rate              | 5 - 45FPS in configurable option modes for distance / accuracy / framerate                                                                                               |
| Exposure Time     | 4.8 ms typ. @ 45 fps / 30 ms typ. @ 5 fps                                                                                                                                |
| Resolution        | 240 x 180 px                                                                                                                                                             |
| FOV (H x W)       | 106° x 86°, 138.4° DFOV Max                                                                                                                                              |
| Range             | 4 - 6m                                                                                                                                                                   |
| Illumination      | 940nm                                                                                                                                                                    |
| Depth Resolution  | TBD                                                                                                                                                                      |
| Time Sync         | No physical pin, but the frame timestamp is measured with 50ns precision on a single clock. All of the sensors on the VOXL platform are timestamped for computer vision. |
| Power Consumption | TBD                                                                                                                                                                      |
| Weight            | TBD                                                                                                                                                                      |
| Dimensions        | TBD                                                                                                                                                                      |
| Eye Safe          | Yes                                                                                                                                                                      |


### Current Consumption

The module consumes an average of ~3watts due to 1.2-1.4Amp bursts at 3.3V. Some SW settings may reduce or increase this slightly. M0171 is designed to support 1.6A-1.8A bursts, by using a 3A DC/DC from the 5V input.



## 2D/3D Drawings of Module

* 2D Envelope Dimensions: TBD
* 3D Model (STEP): [M0171-1 PCB](https://storage.googleapis.com/modalai_public/modal_drawings/M0171_PMD_TOF_HP_ADAPTER_REVA(-1_MAIN).step)
* 3D Model (STEP): [PMD IRS2975C Module](https://storage.googleapis.com/modalai_public/modal_drawings/PMD_IRS2975C_MODULE.step)

### Drawings and Images

#### Image of Working Module



#### Render of Module Stack

![M0171-render.jpg](/images/other-products/image-sensors/TOF_Render_dualview.jpg)

#### Render of Module Top

![M0171-1-top.jpg](/images/other-products/image-sensors/m0171-1-top.JPG)

#### Render of Module Bottom

![M0171-1-bot.jpg](/images/other-products/image-sensors/m0171-1-bot.jpg)
Note the bottom side has no components on it, thus allowing for thermal material or heat spreader to sit nicely between the M0171 CCA and the PMD TOF module.
Be careful of the large exposed test points if placing material directly on the back of M0171 to aviod shorts.

## Pin-out

### Pin-out P1 

#### Connector Specs
The 40-pin connector on M0171 is the new future format ModalAI will slowly be transitioning to that will replace the 24-pin and 36-pin connectors currently used across our image sensor line. This change will take place slowly, but it is a welcome improvement and capability enhancement.

The family from Molex is referred to as the Slimstack SSB RP series, and is a 0.35mm fine pitch B2B style, but includes a 3.0A power delivery contact system in addition to the noted pin counts. So, the 40-pin family we use provides 40 signal/GND pins and a 3.0A capable power feed system, which we define with 5V.

We defined the PLUG to be at the sensors, and the Receptacle to be on the Host (VOXL 2) side. This is more common in industry where the plug is free floating in space, and the receptacle is more "fixed" in place. We will use "Px" and "Jx" reference designators accordingly. The default mating height of this series as we use them are 0.8mm, but there are limited combinations that support 0.9mm.


| VOXL 2 Board Connector (via an adapter such as M0172/M0173)                                                                                          | M0171-1 P1 Connector                                                                                  |
|---------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| Molex, Receptacle MPN 5052704012, 0.8mm mate height | Molex, Plug MPN 5052744012 |
| Molex, Alternate Receptacle MPN 5054134010, 0.9mm mate height| Molex, Plug MPN 5052744012 |

#### Pin-out at P1

| Pin # | Signal Name            | Pin # | Signal Name                             |
|-------|------------------------|-------|-----------------------------------------|
| 41    | GND (NAIL_1a)          | 42    | GND (NAIL_1b)                           |
| 1     | N.C.                   | 2     | N.C.                                    |
| 3     | VREG_L7_1P8 (DOVDD)    | 4     | VREG_3P3V_LOCAL                         |
| 5     | N.C.                   | 6     | N.C.                                    |
| 7     | N.C.                   | 8     | GND                                     |
| 9     | GND                    | 10    | N.C.                                    |
| 11    | CSI_CLK_P              | 12    | N.C.                                    |
| 13    | CSI_CLK_N              | 14    | N.C.                                    |
| 15    | CSI_LANE0_P            | 16    | N.C.                                    |
| 17    | CSI_LANE0_N            | 18    | GND                                     |
| 19    | CSI_LANE1_P            | 20    | RESET_N (Unused, pulled HIGH on M0171)  |
| 21    | CSI_LANE1_N            | 22    | N.C.                                    |
| 23    | GND                    | 24    | CCI_I2C_SDA (1.8V DOVDD levels)         |
| 25    | N.C.                   | 26    | CCI_I2C_SCL (1.8V DOVDD levels)         |
| 27    | N.C.                   | 28    | N.C.                                    |
| 29    | N.C.                   | 30    | N.C.                                    |
| 31    | N.C.                   | 32    | GND                                     |
| 33    | GND                    | 34    | MCLK (1.8V DOVDD levels)                |
| 35    | N.C.                   | 36    | N.C.                                    |
| 37    | VREG_3P3V_LOCAL        | 38    | N.C.                                    |
| 39    | N.C.                   | 40    | N.C.                                    |
| 43    | VDC_5V_LOCAL (NAIL_2a) | 44    | VDC_5V_LOCAL (NAIL_2b)                  |

Pay close attention to the pinout details in this image so that any custom hardware design maps pin 1 to pin 1, including the power nail features: 

![M0171-connector-pinout.jpg](/images/other-products/image-sensors/40-pin-connector-guidance.jpg)
Note the Power Nail pins are numbered 41, 42, 43, and 44 and are on both sides of the connector, with 41/42 being nearer to the Pin 1 side.

## Hardware Design Guidance

If designing your own custom hardware, here are other things to keep in mind to adapt to M0171:

* All MIPI CSI Lines are to be impedance controlled at 100-ohms differential.

* * Match the _P/_N skew to within 0.2mm, and all diff pairs within the group to be +/- 0.5mm
* * Keep the overall length of the MIPI CSI lines to be less than 120mm or a re-driver may be required
* VOXL 2 already has 2.2K pullups on CCI I2C lines to DOVDD
* The 5V must be heavily bypassed with bulk capacitors, and provide several vias throughout the power route flow to support sustained 2A draws
* Starling will incorporate a thermal spreader on the TOF module, and we recommend your design plans for some type of thermal allocation as well to deal with the ~3W consumption of the PMD module
* Be sure to complete a 3D fit check using our 3D models linked above. We have several caps very close to the connector, and with a 0.8mm mate height, care must be taken to avoid interferences from components.
* RESET_N noted above in the pinout table is held HIGH on M0171. We do not expect to ever need to control it with a GPIO/RESET_N signal, but if making your own hardware, it may be worthwhile to provide this feature should we decide to use it in the future.
* We have plans to connect this TOF adapter to both LOWER and UPPER CSI ports throughout VOXL 2, so please check the camera configs to make sure we have your specific CSI port enabled for this TOF before committing to design tapeout. 


## VOXL SDK Usage

Note: This guide assumes that you are able to run commands on VOXL2; if you cannot, please see [VOXL Developer Bootcamp](https://docs.modalai.com/voxl-developer-bootcamp/). All commands specified below should be run on VOXL2. 

### Supported Sensor Module

* M0171-1 IRS2975C Module (Requires VOXL SDK >= v1.2)

### IRS2975C Camera Server Configuration

Note: IRS2975C support is coming to VOXL camera server in VOXL SDK release 1.2

In order to set up your VOXL2 to use just the IRS2975C ToF sensor, connect the camera to the J6 Lower camera connector (it must have ID 0; for more details, see [VOXL2 Camera Configs](https://docs.modalai.com/voxl2-camera-configs/)). Then, run `voxl-configure-cameras 24` in order to configure VOXL camera server to use the M0169-1 ToF sensor.

If you wish to use any of the pre-existing M0040-1 ToF configurations listed in the [VOXL2 Camera Configs](https://docs.modalai.com/voxl2-camera-configs/) page, first run `voxl-configure-cameras <id>`, where `<id>` is the ID number of the configuration you wish to use. Then, edit the VOXL Camera Server configuration file on VOXL2 (`/etc/modalai/voxl-camera-server.conf`) and replace the line containing `"type": "pmd-tof",` with `"type": "pmd-tof-liow2",`. You have now updated a legacy M0040-1 ToF configuration to function with the newer M0171-1 module.

After performing either of the above procedures, please reboot VOXL2 in order to reload the VOXL camera server configuration. To verify functionality, please see the [VOXL Portal section](#VOXL-Portal) below.

### VOXL Portal

In order to verify the functionality of your ToF configuration, you can attempt to view the IR and depth map images in VOXL portal. In order to view VOXL portal, your VOXL should be connected to the internet (see [VOXL2 WiFi Setup](https://docs.modalai.com/voxl-2-wifi-setup/)). 

After your VOXL is connected to the internet, run `systemctl status voxl-portal`. Then, find the IP address of your VOXL using `hostname -I` and visit this address in the web browser of a computer connected to the same local network as your VOXL (or your VOXL's access point, in AP mode). Then, navigate to "Cameras > TOF Conf / Depth / IR" in the top navigation bar, and verify that the displayed video stream matches what the camera is pointed at. 

## Example Code

The best approach to access TOF data on VOXL is to write an MPA client that listens to the voxl-camera-server pipe data. An example of that is [here](https://gitlab.com/voxl-public/ros/voxl_mpa_to_ros/-/blob/master/src/interfaces/tof_interface.cpp).

### Lower-level Examples

[HAL3 TOF Point Cloud Publishing to MPA Pipe in voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server/-/blob/master/src/api_interface/hal3/hal3_camera_mgr_tof.cpp)

