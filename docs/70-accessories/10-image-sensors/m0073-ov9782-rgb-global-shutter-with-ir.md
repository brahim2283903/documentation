---
layout: default
title: M0073 OV9782 Module Datasheet (with IR Filter)
parent: Image Sensors
nav_order: 73
has_children: false
permalink: /M0073/
---

# VOXL RGB Global Shutter Stereo Sensor Module

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

The M0073-1 is an RGB, global shutter, stereo sensor compatible with ModalAI's [M0010-1](m0010) and [M0039-1](m0039) stereo flex cables.

The M0073-1 *does* incorporate an IR-cut filter.

The M0073-1 is identical to [M0113-1](/m0113/) except that M0073-1 incorporates an IR-cut filter.

[Buy Here](https://www.modalai.com/M0073)

| Specification | Value                   |
|----------------|-------------------------|
| Sensor         | OV9782                  |
| Shutter        | Global Shutter          |
| Resolution     | 1280x800                |
| Framerate      |                         |
| Lens Size      |                         |
| Focusing Range |                         |
| Focal Length   | 1.56 +/- 5%             |
| F Number       | 2.2 +/- 5%              |
| Fov(DxHxV)     | 121.3° x 103.2° x 84.6° |
| TV Distortion  | 10%                     |
| Weight         |                         |
| IR Filter      | IR-cut Filter           |


## 2D Drawing

![M0073-2d](/images/other-products/image-sensors/m0113-2d.jpg)

## Lens Sepcification

![M0073-lens-spec](/images/other-products/image-sensors/m0073-lens-spec.jpg)