---
layout: default
title: M0026 IMX377 Module Datasheet
parent: Image Sensors
nav_order: 26
has_children: false
permalink: /M0026/
---

# VOXL Hires Sensor Datasheet

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification


### M0026-1 17cm IMX377 100° FOV ([Buy Here](https://www.modalai.com/M0026))

| Specification | Value                                                                                         |
|----------------|-----------------------------------------------------------------------------------------------|
| Sensor         | IMX377 [Datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/1150455/SONY/IMX377.html) |
| Shutter        | Rolling                                                                                       |
| Resolution     | 4000x3000                                                                                     |
| Framerate      | up to 60Hz                                                                                    |
| Lens Size      | 1/2.3"                                                                                        |
| Focusing Range | 50cm~infinity                                                                                 |
| Focal Length   | 3.24mm                                                                                        |
| F Number       | 2.0                                                                                           |
| Fov(DxHxV)     | ~100°                                                                                         |
| TV Distortion  |                                                                                               |
| Weight         | 10g                                                                                           |
| IR Filter      | 650nm IR FILTER                                                                               |

