---
layout: default
title: M0113 OV9782 Module Datasheet
parent: Image Sensors
nav_order: 113
has_children: false
permalink: /M0113/
---

# VOXL RGB Global Shutter Stereo Sensor Module

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Specification

The M0113-1 is an RGB, global shutter, stereo sensor compatible with ModalAI's [M0010-1](m0010) and [M0039-1](m0039) stereo flex cables.

The M0113-1 does NOT incorporate an IR-cut filter.

The M0113-1 is identical to [M0073-1](/m0073/) except that M0113-1 does NOT incorporate an IR-cut filter.

[Buy Here](https://www.modalai.com/M0113)

| Specification | Value                   |
|----------------|-------------------------|
| Sensor         | OV9782                  |
| Shutter        | Global Shutter          |
| Resolution     | 1280x800                |
| Framerate      |                         |
| Lens Size      |                         |
| Focusing Range |                         |
| Focal Length   | 1.56 +/- 5%             |
| F Number       | 2.2 +/- 5%              |
| Fov(DxHxV)     | 121.3° x 103.2° x 84.6° |
| TV Distortion  | 10%                     |
| Weight         |                         |
| IR Filter      | No IR-cut Filter        |


## 2D Drawing

![m0113-2d](/images/other-products/image-sensors/m0113-2d.jpg)

## Lens Sepcification

![m0113-lens-spec](/images/other-products/image-sensors/m0113-lens-spec.jpg)