---
layout: default
title: M0014 Tracking Module Datasheet
parent: Image Sensors
nav_order: 14
has_children: false
permalink: /M0014/
---

# VOXL Tracking Sensor Datasheet
{: .no_toc }

## MSU-M0014-1-01

### Specification

| Specicifcation | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OV7251 [Datasheet](https://www.ovt.com/wp-content/uploads/2022/01/OV7251-PB-v1.9-WEB.pdf) |
| Shutter        | Global                                                                               |
| Resolution     | 640x480                                                                              |
| Framerate      | 30,60,90Hz implemented on VOXL, sensor suports up to 120Hz                           |
| Data formats   | B&W 8 and 10-bit                                                                     |
| Lens Size      | 1/3.06"                                                                              |
| Focusing Range | 5cm~infinity                                                                         |
| Focal Length   | 0.83mm                                                                               |
| F Number       | 2.0                                                                                  |
| Fov(DxHxV)     | 166° x 133° x 100°                                                                   |
| TV Distortion  | -20.77%                                                                              |

![m0014-tracking-image-sensor-fisheye.jpg](/images/other-products/image-sensors/m0014-tracking-image-sensor-fisheye.jpg)

### Technical Drawings

#### 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0014%20Tracking%20Camera.STEP)

#### 2D Diagram
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0014_2D_11-02-21.pdf)

### Pin Out

![Tracking_Pin1.JPG](/images/other-products/image-sensors/Tracking_Pin1.JPG)

![Tracking_Orientation.JPG](/images/other-products/image-sensors/Tracking_Orientation.JPG)

## Samples of Tracking sensor on Sentinel.
### Indoor
![tracking_in.png](/images/other-products/image-sensors/Samples/tracking_in.png)
{:target="_blank"}

### Outdoor
![tracking_out.png](/images/other-products/image-sensors/Samples/tracking_out.png)
{:target="_blank"}
