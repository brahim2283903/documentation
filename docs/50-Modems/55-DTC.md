---
layout: default3
title: DTC Modems
parent: Modems
nav_order: 55
has_children: true
permalink: /dtc-modems/
thumbnail: /modems/dtc/blusdr-6.png
summary: Documentation for Domo Tactical Communications Modems
---

# ModalAI DTC Modems
{: .no_toc }

{:toc}
Documentation for ModalAI's range of community supported DTC modems, this guide specifically covering the BluSDR-6.  

<a href="https://www.domotactical.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Domo Tactical Support </button></a>

For support please email US.Technical.Support@domotactical.com or UK.Technical.Support@domotactical.com