---
layout: default
title: Sentinel PX4
parent: Sentinel User Guide
nav_order: 22
has_children: false
permalink: /sentinel-user-guide-px4/
youtubeId: aVHBWbwp488
---

1. TOC
{:toc}

# Sentinel PX4
{: .no_toc }

## System Bootup Behavior

PX4 starts automatically after Sentinel boots via the `voxl-px4` service.

## Checking PX4 Status

```bash
voxl-inspect-services
```

The following services are required to be running:

- `voxl-mavlink-server`
- `voxl-vision-hub`
- `voxl-px4`

```
 Service Name         |  Enabled  |   Running   |  CPU Usage
---------------------------------------------------------------
 docker-autorun       | Disabled  | Not Running |
 docker-daemon        | Disabled  | Not Running |
 modallink-relink     | Disabled  | Not Running |
 voxl-camera-server   |  Enabled  |   Running   |   30.0
 voxl-cpu-monitor     | Disabled  | Not Running |
 voxl-dfs-server      | Disabled  | Not Running |
 voxl-imu-server      | Disabled  | Not Running |
 voxl-mavlink-server  |  Enabled  |   Running   |     0.0
 voxl-modem           | Disabled  | Not Running |
 voxl-portal          | Disabled  | Not Running |
 voxl-px4-imu-server  | Disabled  | Not Running |
 voxl-px4             |  Enabled  |   Running   |     0.0
 voxl-qvio-server     | Disabled  | Not Running |
 voxl-static-ip       | Disabled  | Not Running |
 voxl-streamer        | Disabled  | Not Running |
 voxl-tag-detector    | Disabled  | Not Running |
 voxl-tflite-server   | Disabled  | Not Running |
 voxl-time-sync       | Disabled  | Not Running |
 voxl-vision-hub      |  Enabled  |   Running   |     0.0
 voxl-wait-for-fs     |  Enabled  |  Completed  |
 ```

If it is not running, the following wizards can be used:

```bash
voxl-configure-mavlink-server
```

```bash
voxl-configure-vision-px4
```

## PX4 Startup Configuration

The startup configuration is specified here:

```bash
/etc/modalai/voxl.config
```

The service that starts PX4 up on bootup is here:

```bash
/etc/systemd/system/voxl-px4.service
```

[Next Step: Connect to Ground Station](/sentinel-user-guide-connect-gcs/){: .btn .btn-green }
