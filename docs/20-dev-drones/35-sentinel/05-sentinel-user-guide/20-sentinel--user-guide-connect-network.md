---
layout: default
title: Sentinel Connect Network
parent: Sentinel User Guide
nav_order: 20
has_children: false
permalink: /sentinel-user-guide-connect-network/
youtubeId: aVHBWbwp488
---

# Sentinel Connect Network
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Connect Using WiFi

### Related Video

The following video shows how to connect using a development kit:

{% include youtubePlayer.html id=page.youtubeId %}

### voxl-wifi

First, ADB onto your device if you haven't already done so:

```bash
adb shell
```

In order to connect your Sentinel to a Wi-Fi access point, run the following:

```
voxl-wifi station [SSID] [PSK]
```

where `SSID` and `psk` are the credentials for your WiFi network.

In order to connect using SoftAP mode, run the following:
```bash
voxl-wifi softap [SSID]
```

Note: if you use SoftAP mode, you will need to reboot:

```bash
# exit from adb
exit

# reboot
adb reboot
```

### Check IP Address

Get the Sentinel shell over adb again:

```bash
adb shell
```

Run the following to find the IP of the Sentinel:

```bash
ifconfig wlan0
```

You'll see something like:

```
wlan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.188  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::7970:262b:29b8:37e9  prefixlen 64  scopeid 0x20<link>
        ether 00:03:7f:12:33:6e  txqueuelen 3000  (Ethernet)
        RX packets 51  bytes 7204 (7.2 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 18  bytes 1889 (1.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

In this case, Sentinel's IP address on the network is `192.168.1.188`

### SSH onto Sentinel

Now, from you host machine, we can SSH on target using the following instead of adb:

```
ssh root@192.168.1.188
```

The default SSH password is `oelinux123`

If the SSH connection is successful, you should see the following:

```
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.19.125-perf aarch64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.

   https://ubuntu.com/blog/microk8s-memory-optimisation
This system has been minimized by removing packages and content that are
not required on a system that users do not log into.

To restore this content, you can run the 'unminimize' command.

The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

```
If you are having issues SSHing into the system as root, check [here](/voxl2-networking-quickstart/).
## Connecting to 5G network

### Install SIM Card

To install a SIM into Sentinel, start by disconnecting the battery.

![RB5-SIM-4](/images/rb5/rb5-sim-install-4.jpg)

Then, remove the two screws holding in the flight deck lid.
<hr>

![RB5-SIM-2](/images/rb5/rb5-sim-install-2.jpg)

Next, remove the 4 screws holding in the chirp sensor. You may need to hold the bolts in place from the inside while unscrewing from the outside.   
<hr>

![RB5-SIM-1](/images/rb5/rb5-sim-install-1.jpg)

 The SIM slot is located under the top PCB as shown above. Slide the SIM into the slot until you hear the click.
You  might find this process easier using forceps.
<hr>

![RB5-SIM-3](/images/rb5/rb5-sim-install-3.jpg)
Make sure the SIM is fully inserted. If you ever want to swap the SIM, just push on it, and it will pop out.
<hr>

![RB5-SIM-4](/images/rb5/rb5-sim-install-4.jpg)
Screw the chirp sensor back in place. Then, screw the flight deck lid back on the same way you took it off.
<hr>

### Configuring Service

In order to establish a connection to the 5G network, `voxl-modem` is required.  The `voxl-modem` service can be configured by running the following:

```
voxl-configure-modem
```

The user will be prompted to select their modem hardware and APN (Access Point Name) for their chosen SIM card.

This will enable the `voxl-modem` service in the background and it will be started at boot.


### Disabling / enabling service

The `voxl-modem` service can be disabled by running the following:

```
systemctl disable voxl-modem
```

It can be re-enabled using: 

```
systemctl enable voxl-modem
```

The status of the service can be viewed by running:

```
systemctl status -l voxl-modem
```

### Next Steps

There are roughly three paths you could take:

- Check out PX4 and Fly

[Next Step: PX4](/sentinel-user-guide-px4/){: .btn .btn-green}

- For developers, check out our Developer and Build Guides

[Next Step: Developer Guide](/voxl-px4-developer-guide/){: .btn .btn-green} 
[Next Step: Build Guide](/voxl2-px4-build-guide/){: .btn .btn-blue .float-left}

- Skip to Machine Vision features to mess around at your desktop

[Next Step: Use VIO](/sentinel-user-guide-using-vio/){: .btn .btn-green}


