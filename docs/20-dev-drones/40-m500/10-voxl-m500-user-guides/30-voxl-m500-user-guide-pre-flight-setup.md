---
layout: default
title: VOXL m500 Pre-Flight Setup
parent: M500 User Guides
nav_order: 20
has_children: false
permalink: /voxl-m500-user-guide-pre-flight-setup/
---

1. TOC
{:toc}

# m500 Pre-Flight Setup
{: .no_toc }

## Sensor Calibration
VOXL-M500 flight sensors are delivered pre-calibrated from the factory. PX4 will warn you if your compass needs to be re-calibrated depending on your location and environmental conditions. If so, or if you'd like re-calibrate all sensors, follow the [px4 sensor calibration precedure](/px4-calibrate-sensors/).

## RC Radio Setup

Now you need to bind and calibrate your DSM RC radio through the qGroundControl interface.

These instructions focus on the default receiver using Spektrum DSMx. For other radio types see the [Flight Core Radios Page](https://docs.modalai.com/flight-core-radios/) and [Flight Core Connections Page](/flight-core-connections/#rc-receiver) for more details.

### Bind to Radio

#### Spektrum (Default on m500)
Instructions on how to bind a Spektrum radio can be found [here](https://docs.px4.io/master/en/config/radio.html).

#### FrSky
Video tutorial on how to pair an FrSky X8R [(Youtube)](https://www.youtube.com/watch?v=1IYg5mQdLVI)

### Calibrate Radio

Now follow the on-screen instructions to calibrate the range and trims of your radio.

![8-calibrate-radio.png](/images/voxl-sdk/qgc/8-calibrate-radio.png)

### Confirm RC Settings

Obviously every user will want to use different flight modes and different switch assignments, but for getting started with VOXL and and Flight Core we suggest starting with something similar to this configuration and working from there.

- "Flap Gyro" switch left of Spektrum Logo
  - Channel 6
    - Up position:     Manual Flight Mode
    - Middle Position: Position Flight Mode
    - Down Position:   Offboard Flight mode

- "Aux2 Gov" switch right of Spektrum Logo
  - Channel 7
    - Up position:     Motor Kill Switch Engaged
    - Down Position:   Motor Kill Switch Disengaged (required to fly)

Since we have a manual kill switch on the radio there is no need for the "safety switch" on the Pixhawk GPS module as so it is disabled in our params file in favor of the kill switch.

![9-flight-mode-config.png](/images/voxl-sdk/qgc/9-flight-mode-config.png)

If you have a Spektrum DX6e or DX8 radio with   a clean acro-mode model you can accomplish the above channel mapping by loading the following config file

[https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params](https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params)

Still confirm the mapping in qGroundControl before flight!


[Next Step: First Flight](/voxl-m500-user-guide-first-flight/){: .btn .btn-green }