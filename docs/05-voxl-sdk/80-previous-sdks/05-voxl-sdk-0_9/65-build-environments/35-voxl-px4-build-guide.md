---
layout: default
title: VOXL 2 PX4 Build Guide 0.9
parent: Build Environments 0.9
search_exclude: true
nav_order: 35
permalink:  /voxl2-px4-build-guide-0_9/
---

# VOXL 2 PX4 Build Guide
{: .no_toc }

## PX4 Build Environment Instructions

`This has only been tested with Ubuntu 20.04 Focal Fossa (https://releases.ubuntu.com/20.04/) on an x86_64/AMD64 architecture. This means all ARM systems, including M1 based processors are incompatible with the following process.`  
  
Below is the process in order to build the environment in order to compile px4-firmware surrounding the VOXL2.

- Create an account with qualcomm developer network (https://developer.qualcomm.com/)
- After account has been created and confirmed via email, proceed in the following order on the website above: Downloads --> Software Downloads --> Qualcomm Package Manager
- After Qualcomm Package Manager has been downloaded, proceed to untar the download in whatever directory was used. This can be done by running the following in a terminal/bash window:
    - `cd DOWNLOAD_DIR`
    - `tar -xz qualcommpackagemanager.lnx_.2.0_installer_20021.2.tar` 
- Proceed to run the following post untarring the file above.
    - `sudo apt-get update`
    - `sudo apt-get upgrade`
    - `sudo apt --fix-broken install`
    - `sudo apt-get install xterm`
    - `wget http://launchpadlibrarian.net/416685704/multiarch-support_2.19-0ubuntu6.15_amd64.deb`
    - `wget http://launchpadlibrarian.net/201380610/libgnome-keyring0_3.12.0-1build1_amd64.deb`
    - `wget http://launchpadlibrarian.net/201380608/libgnome-keyring-common_3.12.0-1build1_all.deb`
    - `sudo dpkg -i multiarch-support_2.19-0ubuntu6.15_amd64.deb`
    - `sudo dpkg -i libgnome-keyring-common_3.12.0-1build1_all.deb`
    - `sudo dpkg -i libgnome-keyring0_3.12.0-1build1_amd64.deb`
    - `sudo dpkg -i QualcommPackageManager.2.0.21.1.Linux-x86.deb`

It is now time to install and archive the necessary Hexagon SDK - `version 4.1.0.4 is required, later versions are not compatible with code base as of now (tested)`

- In terminal, proceed to run the following: `sudo qpm`
- Please wait a few minutes as it takes some time for this GUI to load.
- Once GUI has loaded, proceed to select the Hexagon SDK, ensure that in the drop downs, you select version 4.1.0.4 and proceed to select install on the other drop down. This will prompt you to install to a default directory.
- Once this is done, proceed to tar up this directory by typing the following: `tar -czvf sdk.tgz DIRECTORY_TO_SDK/4.1.0.4/`
- Move this file into the rb5 directory by typing the following: `mv DIR_TO_SDK/sdk.tgz DIR_TO_RB5/rb5-flight-px4-build-docker `
- Once here, it is time to begin building the docker image. This is done in a three step process:
    1. Run `./setup.sh`
    2. Run `./build.sh`
    3. Run `./run.sh`
NOTE: If docker is not within the USER group, it will require you to run these commands in sudo.

- Once inside the interactive docker terminal - proceed to clone down the modalAI px4-firmware git repository. This can be done by typing the following: `git clone https://github.com/modalai/px4-firmware`

- Once cloned, proceed to do the following steps:
    1. Run `source /home/build-env.sh`
    2. Run `cd px4-firmware`
    3. Checkout whichever branch you intend on using
    4. Run `make clean`
    5. Run whichever board firmware you plan on running PX4 on
