---
layout: default
title: VOXL PX4 User Guide 0.9
parent: VOXL PX4 0.9
search_exclude: true
nav_order: 10
permalink:  /voxl-px4-user-guide-0_9/
---

# VOXL PX4 User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Introduction

<<<<<<< HEAD
voxl-px4 is a service that runs on the apps processor (ie CPU) on VOXL 2 that handles the CPU-side PX4 processing, launches the sDSP PX4 RTOS implementation, and handles the communication between CPU and sDSP.

If you are interested in developing for the sDSP, see the [VOXL 2 PX4 Developer Guide](/voxl-px4-developer-guide/) and [VOXL 2 PX4 Build Guide]((/voxl-px4-build-guide/))

### Block Diagram
=======
This guide is for `voxl-px4` as shipped in VOXL 2 and VOXL 2 Mini Platform Release 1.0 and newer running 1.14 based PX4.
>>>>>>> documentation/master

voxl-px4 is a service that runs on the apps processor (ie CPU) on VOXL 2 and VOXL 2 Mini that handles the CPU-side PX4 processing, launches the sDSP PX4 RTOS implementation, and handles the communication between CPU and sDSP.

If you are interested in developing for the sDSP, see the [VOXL 2 PX4 Developer Guide](/voxl-px4-developer-guide/) and [VOXL 2 PX4 Build Guide]((/voxl-px4-build-guide/))

# Hardware

## Supported Devices

The `voxl-px4` package is supported on the following platforms:

- VOXL 2 Mini (board ID `M0104`)
- VOXL 2  (board ID `M0054`)
- RB5 Flight  (board ID `M0052`)

This guide is geared towards someone trying to use the PX4 system, and not necessarily interested in all the bits and bytes that make it work.  If you want to know the inner workings, please see the [VOXL PX4 Developer Guide](/voxl-px4-developer-guide/)

## Hardware Block Diagrams

Below are hardware block diagrams that show the VOXL 2 and VOXL 2 Mini in supported architectures used to facilitate robotics applications.  Many other variations are possible.

### VOXL 2 Mini Based Setup

[View in fullsize](/images/voxl2-mini/m0104-px4-user-guide-overview-v1.jpg){:target="_blank"}
<img src="/images/voxl2-mini/m0104-px4-user-guide-overview-v1.jpg" alt="m0104-px4-user-guide-overview-v1"/>

#### Minimum Setup Option

Minimum requirements are (e.g. to play around at your desk)

#### Setup to Connect to Ground Control Station

Additionally/Optionally (e.g. to connect to Ground Control Station):

### VOXL 2 Based Setup

[View in fullsize](https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1.jpg){:target="_blank"}
<img src="https://gitlab.com/voxl-public/support/drawings/-/raw/master/D0006/D0006-C11-M13-T1.jpg" alt="D0006-C11-M13-T1"/>

#### Minimum Setup Option

Minimum requirements are (e.g. to play around at your desk)

- VOXL 2 Development Kit (MDK-M0054-1-01)
  - VOXL 2 (MDK-M0054-1-00)
  - VOXL Power Module (MDK-M0041-1-B-00)
  - Power cable (MCBL-00001-1)
- Power supply 12V 3A ((MPS-00005-1), XT60) (or 2S-6S battery)
  - *Note: inrush current on bootup requires 30W power supply min, nominally ~8W at runtime in beta*
- Host PC with [Android Debug Bridge]()
- USBC cable

#### Setup to Connect to Ground Control Station

Additionally/Optionally (e.g. to connect to Ground Control Station):

- Add-on board for networking:
  - [USB Expansion Board](https://www.modalai.com/products/voxl-usb-expansion-board-with-debug-fastboot-and-emergency-boot) (M0017) + JST-to-USB cable (MCBL-00009-1) + [Ethernet](/voxl2-connecting-quickstart/) or [Wi-Fi dongle](/voxl2-wifidongle-user-guide/)
  - 5G Modem Add-on (M0090-3-01) + your own VPN
  - [Microhard Modem Add-On](https://www.modalai.com/products/voxl-microhard-modem-usb-hub) (M0048-1)

#### Setup for Bringup and Manual Flight

Additionally/Optionally (e.g. to fly manual modes, testing and bringup):

- [ModalAI 4-in-1 UART ESC](https://www.modalai.com/products/voxl-esc) (M0049)
- GPS/Mag/Spektrum RC input wiring harness (MSA-D0006-1-00) - or make your own 12-pin JST GH connector ([pinouts](/voxl2-connectors/))
- the actual GPS/Mag/Spektrum RC* receiver hardware
- Frame/Motors/Props/etc

*Note: only Spektrum receivers supported in beta*

## ESC Configs

Information about ESC configs:
- [VOXL 2 Mini](/voxl2-mini-esc-configs/)
- [VOXL 2](/voxl2-esc-configs/)

## RC Configs

Information about RC configs:
- [VOXL 2 Mini](/voxl2-mini-rc-configs/)
- [VOXL 2](/voxl2-rc-configs/)

## Onboard Sensors

Information about onboard sensors:
- [VOXL 2 Mini](/voxl2-mini-onboard-sensors/)
- [VOXL 2](/voxl2-onboard-sensors/)

## Offboard Sensors

Information about offboard sensors:
- [VOXL 2 Mini](/voxl2-mini-offboard-sensors/)
- [VOXL 2](/voxl2-offboard-sensors/)

## Autopilot Orientation

Below are the default autopilot orientations.  To update for your setup, use the `SENS_BOARD_ROT` parameter or configure using a Ground Control Station like QGC.

<img src="/images/voxl2-mini/voxl-px4-user-guide-autopilot-orientation.jpg" alt="voxl-px4-user-guide-autopilot-orientation"/>

# Software

## Key PX4 Differences to Note

- PX4 installation and updates are installed on the companion computer via the `apt` package manager (not QGroundControl)
- Currently, the USBC connection does not support a connection to QGroundControl
- Natively, ESCs are control is via UART, if you need PWM, check out [voxl2-io](/voxl2-io)
- the MAVLink shell available through QGC doesn't currently work on VOXL 2.  Instead, use the USBC connection and adb to access the shell if needing to run commands

## Enable voxl-px4 Daemon

Connect to VOXL2 / VOXL 2 Mini over USB/adb (see [here](/voxl2-mini-usb-connections/)) and run the following helper:

```
voxl-configure-px4 factory_enable
```

*Example output from running command for reference:*

```
voxl2-mini:/$ voxl-configure-px4 factory_enable
enabling  voxl-px4 systemd service
Created symlink /etc/systemd/system/multi-user.target.wants/voxl-px4.service → /etc/systemd/system/voxl-px4.service.
Done configuring voxl-px4
```

Reboot VOXL 2 / VOXL 2 Mini from the host computer.

```
adb reboot
```

## px4 Commands

After rebooting, `voxl-px4` will run on bootup if the service is enabled (like we did in the last step).  

With the `voxl-px4` daemon running, you can run some common PX4 commands like "listener" with a `px4-` prefix.

For example, `px4-listener sensor_accel` shows the following output:

```
voxl2-mini:/$ px4-listener sensor_accel

TOPIC: sensor_accel
 sensor_accel
    timestamp: 4963582637 (9.119637 seconds ago)
    timestamp_sample: 4963582379 (258 us before timestamp)
    device_id: 2490378 (Type: 0x26, SPI:1 (0x00))
    x: -0.26791
    y: -0.06608
    z: 9.83514
    temperature: 35.03019
    error_count: 1
    clip_counter: [0, 0, 0]
    samples: 10
```

## voxl-mavlink-server Daemon

### Enable Daemon

`voxl-px4` communicates over IP to the outside world using the `voxl-mavlink-server` daemon.  To enable, run the following:

```
voxl-configure-mavlink-server factory_enable
```

*Example output from running command for reference:*

```
voxl2-mini:/$ voxl-configure-mavlink-server factory_enable
wiping old config file
Created new json file: /etc/modalai/voxl-mavlink-server.conf
enabling  voxl-mavlink-server systemd service
Done configuring voxl-mavlink-server
```

### IP Connection

- To get an IP link on VOXL 2, see [here](/voxl2-wifidongle-user-guide/)
- To get an IP link on VOXL 2 Mini, see [here](/voxl2-mini-usb-connections/) for options to get a USB connection going, and use some network adapter like a WiFi dongle (e.g. `awus036acs`).

Note: the adb connection does not pass IP traffic and can't be used for this link.

### GCS Connection using WiFi - Using SoftAP

If using a WiFi dongle, an easy way to get a connection going is to setup VOXL 2 / VOXL 2 Mini as a Soft AP (access point) and have your host computer connect wirelessly to it.

When in this mode, VOXL 2 Mini will provide your host PC an address of `192.168.8.10` by default.

The default IP address `voxl-mavlink-server` will use for the main connection to GGC.

So by default, a connection between VOXL 2 / VOXL 2 Mini and your GCS should be facilitated in this mode.

### GCS Connection using WiFi Station Mode or Ethernet Adapter

If using WiFi in Station Mode or an Ethernet Adapter, connect VOXL 2 / VOXL 2 Mini to the same network as your GQC and update the configuration file's `primary_static_gcs_ip` field at `/etc/modalai/voxl-mavlink-server.conf`

## PX4 Parameters

### Default Parameters

The default params for `voxl-px4` are configured [here](https://github.com/PX4/PX4-Autopilot/blob/main/boards/modalai/voxl2/target/voxl-px4-set-default-parameters.config)

Upon first time run, these are the params that are configured.

### Viewing/Editing Parameters

You can use the param subsystem to interact with the parameters, e.g. viewing:

```
px4-param show
```

![voxl-param-show.gif](/images/voxl-sdk/voxl-px4/px4-param-show.gif)
