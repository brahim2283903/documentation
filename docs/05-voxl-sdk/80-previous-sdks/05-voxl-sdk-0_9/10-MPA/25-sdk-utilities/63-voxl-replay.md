---
layout: default
title: VOXL Replay 0.9
parent: SDK Utilities 0.9
search_exclude: true
nav_order: 63
permalink: /voxl-replay-0_9/
---

# VOXL Replay
{: .no_toc }

Example using a voxl-logger file extraced to /home/root/log0003

```
$ mkdir -p /etc/modalai/
$ mkdir -p /data/modalai/
$ cd /home/root
$ cp log0003/etc/modalai/* /etc/modalai/
$ cp log0003/data/modalai/* /data/modalai/
$ voxl-replay -p /home/root/log0003/
```

