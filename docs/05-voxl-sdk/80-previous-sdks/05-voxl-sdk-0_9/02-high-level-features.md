---
layout: default
title: High Level Features 0.9
parent: VOXL SDK 0.9
search_exclude: true
nav_order: 2
has_children: true
has_toc: true
permalink: /high-level-features-0_9/
---

# High Level Features
{: .no_toc }

## Table of contents
{: .no_toc }

1. TOC
{:toc}

## Overview

These are the primary features and functions provided by the [VOXL SDK](/voxl-sdk/) which are designed as starting points to help you accelerate OEMs in the development of their own final products.


<!-- | Feature                    |                                     Documentation |
|:---------------------------|--------------------------------------------------:|
| Interprocess Communication |                                     [link](/mpa/) |
| VIO                        |                                     [link](/vio/) |
| VOA                        |                                     [link](/voa/) |
| Apriltag Detection         |                       [link](/voxl-tag-detector/) |
| Apriltag Relocalization    | [link](/voxl-vision-hub-apriltag-relocalization/) |
| Deep Learning              |                      [link](/voxl-tflite-server/) |
| Mapping                    |                             [link](/voxl-mapper/) |
| Path Planning              |                             [link](/voxl-mapper/) |
| Mavlink Routing            |               [link](/voxl-vision-px4-telemetry/) |
 -->
