---
title: librc_math
layout: default
parent: Core SDK Libraries
has_children: false
nav_order: 20
permalink: /librc-math/
---


# librc_math
{: .no_toc }

`librc_math` is a fork of the [librobotcontrol](https://github.com/beagleboard/librobotcontrol) project that strips away everything except the math portion. It is now maintained independently and provides core math functions for projects such as [`voxl-vision-hub`](/voxl-vision-hub/). The voxl package contains borth 32 and 64 bit shared object libraries as well as the plethora of testing and benchmarking tools in 64-bit form.


Since `librc_math` is a fork, the original Robot Control library documentation is valid and available at [strawsondesign.com/docs/](http://strawsondesign.com/docs/librobotcontrol/group___math.html).


However, it is evolving and new functions are added regularly. The headers are self-documenting, up-to-date, and available on [Gitlab](https://gitlab.com/voxl-public/core-libs/librc_math).


The following test programs are available as part of the package, they all serve as example usage of the library:

# FACT CHECK THIS

```
yocto:/$ rc_ {TAB}
rc_benchmark_algebra           rc_test_matrix
rc_test_algebra                rc_test_polynomial
rc_test_complementary_filters  rc_test_quaternion
rc_test_filters                rc_test_vector
rc_test_kalman
```
