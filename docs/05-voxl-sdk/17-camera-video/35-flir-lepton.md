---
title: FLIR Lepton
layout: default
parent: Cameras & Video
has_children: true
nav_order: 35
permalink: /voxl-lepton-server/
---

# Overview

The voxl-lepton server publishes thermal images from Flir Lepton, connected via SPI and i2c, to the Modal Pipe Architecture as a camera. Currently only for QRB5165-based platforms (VOXL2).

If you are connecting a Lepton via USB, please see [voxl-uvc-flir](https://docs.modalai.com/voxl-uvc-flir/)

Supports the following Flir cameras:
* Lepton V3.5


# Source Code

[Source Code](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-lepton-server/)