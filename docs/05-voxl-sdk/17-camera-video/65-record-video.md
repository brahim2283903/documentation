---
title: Record Video
layout: default
parent: Cameras & Video
has_children: true
nav_order: 65
permalink: /voxl-record-video/
---

# Record Video

voxl-record-video is a simple utility to record the encoded output of [voxl-camera-server](/voxl-camera-server/) to disk. If you want to use the built-in functionality to record video from QGroundControl, see [voxl-mavcam-manager](/voxl-mavcam-manager/)

Encoded output is enabled by default on the hi-res cameras via the configuration file in [voxl-camera-server](/voxl-camera-server/). The name of the pipe this encoded data is being sent to is: `hires_small_encoded` which will be available when voxl-camera-server is configured and running, which should be the case by default.

Like streaming low latency over h264 at 720p/4K, there is also the capability now of doing the same thing via this record feature. This once again, has the ability to compress image frames using OMX for a no-copy hardware accelerated and memory optimized data path to publish this compressed and encoded data. This encoded video is then saved on disk using this record feature. The following parameters are how a user can change the resolution and bitrate in voxl-camara server:

1. `record_width`<br />
2. `record_height`<br />
3. `record_bitrate`<br />

To save the video, on SDK 0.9.5 or higher, the user can open a terminal window on the voxl2 and type the following `voxl-record-video hires_record -o DIRECTORY/OF/OUTPUTVIDEO.h264`. To stop recording, in the same terminal window, simply type `ctrl + c`.

[Source Code](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/-/blob/master/tools/voxl-record-video.c)
