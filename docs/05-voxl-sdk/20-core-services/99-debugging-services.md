---
title: Debugging Services
layout: default
parent: Core Services
has_children: true
nav_order: 99
permalink: /debugging-services/
---


# Debugging Core Services

Each VOXL service found with voxl-inspect-service is a standard Linux [systemd](digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units) service. To debug, disable the service and launch the executable directly from the command line. There are likely more prints available to stdout/stderr.

## voxl-inspect-services

The voxl-inspect-services tool provides high-level visibility into which services are running, and how much of the CPU are they utilizing.

![voxl-inspect-services.jpg](/images/voxl-sdk/voxl-inspect-services.jpg)