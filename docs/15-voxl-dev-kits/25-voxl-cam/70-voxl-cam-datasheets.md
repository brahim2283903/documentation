---
layout: default
title: VOXL CAM Datasheet
parent: VOXL CAM
nav_order: 70
permalink: /voxl-cam-datasheet/
---

# VOXL-CAM v1 Datasheet
{: .no_toc }

---

## Helpful Links
{: .no_toc }
- [User Manual, v1](/voxl-cam-v1-manual/)

## Hardware Overview
![voxl-cam-labeled](/images/voxl-cam/voxl-cam-labeled.jpg)

## Dimensions

[3D STEP File](https://storage.googleapis.com/modalai_public/modal_drawings/VOXL_CAM_STEP.zip)

![voxl-cam](/images/voxl-cam/voxl-cam-dimensions.jpg)
[View in fullsize](/images/voxl-cam/voxl-cam-labeled.pdf){:target="_blank"}


## Specifications

| Feature               | Details                                                                               |
|-----------------------|---------------------------------------------------------------------------------------|
| Dimensions            | 100.32mm x 39.57mm x 17.47mm                                                          |
| Weight                | 57.5g - 87g (depending on configuration)                                              |
| Compute               | [VOXL (M0006)](/voxl-datasheets/)                                                     |
| Flight Controller     | Integrated PX4 coming soon on DSP <br>External FlightCore via UART, not included)     |
| Communications        | USB, WiFi, support for [VOXL Add-Ons](/expansion-boards/) including LTE and Microhard |
| Time of Flight Sensor | [PMD (ToF)](/M0040/)                                                                  |
| Tracking Image Sensor | [OV7251 - MSU-M0014](/M0014/)                                                         |
| Stereo Image Sensors  | [OV7251 - OEM-M0015-1-00](/M0015/)                                                    |
| Connectors            | UART (MAVLink) [VOXL J12](/voxl-datasheets-connectors/)                               |
|                       | UART/I2C/GPIO  [VOXL J7](/voxl-datasheets-connectors/)                                |
|                       | UART           [VOXL J11](/voxl-datasheets-connectors/)                               |
