---
layout: default
title: VOXL 2 Image Sensors
parent: VOXL 2 User Guides
nav_order: 7
permalink:  /voxl2-image-sensors/
---

# VOXL 2 Image Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

# Overview

## Supported Architecture

This documentation is valid for VOXL 2, VOXL 2 Mini staring SDK 1.0.

## Image Sensor Drivers Overview

### Driver Components

The primary purpose of image sensor drivers are:

- selecting the hardware location on VOXL 2 / VOXL 2 Mini
- programming the image sensor with register values to support a desired setup

The image sensor driver is broken into two components that support these requiements:

- the `com.qti.sensormodule.xxxx_N.bin` - where `xxxx` is the sensor type, and `N` represents the HW ID (described below)
- the `com.qti.sensor.xxxx_yyyy_zzzz.so` - where `xxxx` is the sensor type and `yyyy_zzzz` are a possible variants

### Sensor Module bin Location

The "Sensor Module Bin" file is responsible for enabling a sensor to be probed.

All the available `com.qti.sensormodule.xxxx_N.bin` files are located at `/usr/share/modalai/chi-cdk`.

For example:

```
voxl2:/usr/lib/camera$ ls /usr/share/modalai/chi-cdk/
imx214  imx377  imx412  imx678  irs1645  ov7251  ov7251-combo  ov7251-combo-flip  ov9782  ov9782-combo  ov9782-combo-flip
```

<p style="color:red">To enable a sensor to be probed, copy its appropriate `com.qti.sensormodule.xxxx_N.bin` file into `/usr/lib/camera`.</p>

### Sensor so Location

The "Sensor so" holds the sensors register settings (roughly).  

We can keep them all in `/usr/lib/camera` without affecting probes, so all options are shipped into this directory.

# VOXL SDK Usage

## SDK 1.0 Usage Example

To automatically generate a valid, default `/etc/modalai/voxl-camera-server.conf` file for a configuration, a helper script

For reference, see an example for [C11 Sentinel](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server/-/blob/sdk-1.0.0/scripts/qrb5165-configure-cameras?ref_type=tags#L247)

```
camera-server-config-helper stereo_front:ov7251:0:1 tracking:ov7251:2 hires2:imx214:3 stereo_rear:ov7251:4:5
```

And in in `/usr/lib/camera/` we'd expect:

```
com.qti.sensormodule.ov7251_combo_0.bin
com.qti.sensormodule.ov7251_combo_1.bin
com.qti.sensormodule.ov7251_2.bin
com.qti.sensormodule.imx214_3.bin
com.qti.sensormodule.ov7251_combo_4.bin
com.qti.sensormodule.ov7251_combo_5.bin
```

# Image Sensor Hardware IDs

## Overview

Image sensor hardware IDs are derived from the kernel's [device tree](https://gitlab.com/voxl-public/system-image-build/meta-voxl2-bsp/-/blob/master/recipes-kernel/linux-msm/files/dts/m0054/m0054-modalai-camera.dtsi).

There are many ways to build interfaces to the VOXL 3 camera groups.

<img src="/images/voxl2/m0054-iamge-sensor-groups-v2.png" alt="m0054-image-sensors-groups">

# Image Sensor Drivers

## SDK 1.1.0 (system image 1.7.x)

All sensormodule binaries shipped in `/usr/share/modalai/chi-cdk`.

```
imx214
imx214-flip
imx335
imx377
imx412
imx412-flip
imx678
imx678-flip
irs1645
ov7251
ov7251-combo
ov7251-combo-flip
ov7251-fsin
ov7251-fsout
ov9782
ov9782-combo
ov9782-combo-flip
```

## SDK 1.0.0 (system image 1.6.2)

All sensormodule binaries shipped in `/usr/share/modalai/chi-cdk`.

```
imx214
imx377
imx412
imx678
irs1645
ov7251
ov7251-combo
ov7251-combo-flip
ov9782
ov9782-combo
ov9782-combo-flip
```

## SDK 0.9.5 (system image 1.5.5)

All sensormodule binaries and sensor so files shipped in `/usr/lib/camera`

```
com.qti.sensormodule.m0_cmk_imx577_cam0.bin
com.qti.sensormodule.m0_cmk_ov9782l_cam0.bin
com.qti.sensormodule.m0_imx214_cam0.bin
com.qti.sensormodule.m0_liteon_ov7251l_cam0.bin
com.qti.sensormodule.m0_pmd_irs1645.bin
com.qti.sensormodule.m1_cmk_ov9782r_cam1.bin
com.qti.sensormodule.m1_liteon_ov7251r_cam1.bin
com.qti.sensormodule.m1_pmd_irs1645.bin
com.qti.sensormodule.m2_cmk_imx577_cam2.bin
com.qti.sensormodule.m2_cmk_ov9782_cam2.bin
com.qti.sensormodule.m2_imx214_cam2.bin
com.qti.sensormodule.m2_imx678_cam2.bin
com.qti.sensormodule.m2_liteon_ov7251_cam2.bin
com.qti.sensormodule.m2_pmd_irs1645.bin
com.qti.sensormodule.m3_cmk_imx377_cam3.bin
com.qti.sensormodule.m3_cmk_imx577_cam3.bin
com.qti.sensormodule.m3_imx214_cam3.bin
com.qti.sensormodule.m3_imx678_cam3.bin
com.qti.sensormodule.m3_pmd_irs1645.bin
com.qti.sensormodule.m4_cmk_imx577_cam4.bin
com.qti.sensormodule.m4_cmk_ov9782l_cam4.bin
com.qti.sensormodule.m4_imx214_cam4.bin
com.qti.sensormodule.m4_imx678_cam4.bin
com.qti.sensormodule.m4_liteon_ov7251l_cam4.bin
com.qti.sensormodule.m4_pmd_irs1645.bin
com.qti.sensormodule.m5_cmk_ov9782r_cam5.bin
com.qti.sensormodule.m5_imx678_cam5.bin
com.qti.sensormodule.m5_liteon_ov7251r_cam5.bin
com.qti.sensormodule.m5_pmd_irs1645.bin
com.qti.sensormodule.sony_imx335.bin
```