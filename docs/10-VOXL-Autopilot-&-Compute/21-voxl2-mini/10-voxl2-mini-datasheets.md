---
layout: default
title: VOXL 2 Mini Datasheets
nav_order: 10
parent: VOXL 2 Mini
has_children: true
permalink: /voxl2-mini-datasheets/
---

# VOXL 2 Mini Datasheets
{: .no_toc }

<img src="/images/voxl2-mini/m0104-hero-top.png" alt="m0104-hero-top"/>

# Mechanical Drawings

## 2D Drawings

![voxl2-mini-imu-locations](/images/voxl2-mini/m0104-2d-imu-diagram.png)
IMU Orientation Image Downloadable File Link [Here](https://storage.googleapis.com/modalai_public/modal_drawings/M0104_VOXL2_MINI_2D_PUBLIC_DWG_03-30-23.PDF)

## 3D Drawings

[3D STEP File](https://storage.googleapis.com/modalai_public/modal_drawings/M0104_VOXL2_MINI_V1_REVA.step)
